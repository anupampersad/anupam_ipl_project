function topEcoBowlers2015(matches, deliveries) {

    let matchID2015 = []

    for (match of matches) {
        if (match.season == 2015) {
            matchID2015.push(match.id)
        }
    }

    let topEcoBowlers = {}
    for (delivery of deliveries) {

        if (matchID2015.includes(delivery.match_id)) {

            if (delivery.bowler in topEcoBowlers) {
                topEcoBowlers[delivery.bowler]['runs'] += parseInt(delivery.total_runs)
                topEcoBowlers[delivery.bowler]['balls'] += 1

            }
            else {
                topEcoBowlers[delivery.bowler] = {}
                topEcoBowlers[delivery.bowler]['runs'] = parseInt(delivery.total_runs)
                topEcoBowlers[delivery.bowler]['balls'] = 1
            }

        }
    }


    // Calculate overs and economy of bowlers
    for (bowler in topEcoBowlers) {
        topEcoBowlers[bowler]['overs'] = parseInt(topEcoBowlers[bowler]['balls'] / 6) + (topEcoBowlers[bowler]['balls'] % 6) / 10
        topEcoBowlers[bowler]['economy'] = parseFloat(topEcoBowlers[bowler]['runs'] / topEcoBowlers[bowler]['overs']).toPrecision(2)

    }


    // Lower economy better player because average of runs given by baller is less
    // hence we need to sort this object according to economy in ascending order 
    // and find top 10 ballers 

    const arrayFromObject = Object.entries(topEcoBowlers)
    const sortedArray = arrayFromObject.sort((x, y) => x[1].economy - y[1].economy)
    const sortedArrayTop10 = sortedArray.slice(0, 10)
    const topEconomicBowler2015 = Object.fromEntries(sortedArrayTop10)

    return topEconomicBowler2015

}

module.exports = topEcoBowlers2015
