// Requiring Dependencies
const fs = require("fs");
const csvtojson = require("csvtojson");
const path = require('path')

// Importing Fuctions
const matchesPerYear = require('./matchesPerYear');
const matchesWonPerTeamPerYear = require('./matchesWonPerTeamPerYear');
const extraRunsConceded2016 = require('./extraRunsConceded2016');
const topEcoBowlers2015 = require('./topEcoBowler2015');

// Defining Paths 
const matchesPath = path.join(__dirname, '../data/matches.csv');
const deliveriesPath = path.join(__dirname, '../data/deliveries.csv');
const outputPath = path.join(__dirname,'../public/output')


const saveAsJson = (fileName,object) =>{
    fs.writeFileSync(`${outputPath}/${fileName}.json`, JSON.stringify(object),'utf-8',(err)=>{
        if (err) console.log(err)
    })
}


// IIFE function to call all the pre-declared functions and save the resuls in /public/output
(async function final(){

    const matches = await csvtojson().fromFile(matchesPath);
    const deliveries = await csvtojson().fromFile(deliveriesPath);


    let solution1 = matchesPerYear(matches);
    // console.log(solution1);
    saveAsJson('matchesPerYear',solution1);

    let solution2 = matchesWonPerTeamPerYear(matches);
    // console.log(solution2); 
    saveAsJson('matchesWonPerTeamPerYear',solution2);

    let solution3 = extraRunsConceded2016(matches,deliveries);
    // console.log(solution3);
    saveAsJson('extraRunsConceded2016',solution3);

    let solution4 = topEcoBowlers2015(matches,deliveries);
    // console.log(solution4);
    saveAsJson('topEconomicBowlers2015',solution4);

})()



