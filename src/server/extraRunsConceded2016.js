function extraRunsConceded2016(matches, deliveries) {

    let matchID2016 = []

    for (match of matches) {
        if (match.season == 2016) {
            matchID2016.push(match.id)
        }
    }

    let extraRuns2016 = {}

    for (delivery of deliveries) {

        if (matchID2016.includes(delivery.match_id)) {
            if (delivery.bowling_team in extraRuns2016) {
                extraRuns2016[delivery.bowling_team] += parseInt(delivery.extra_runs)
            }
            else {
                extraRuns2016[delivery.bowling_team] = parseInt(delivery.extra_runs)
            }
        }
    }

    return extraRuns2016

}

module.exports = extraRunsConceded2016
