function matchesPerYear(matches) {

    matchesPerYearObject = {}
    for (match of matches) {
            if (match.season in matchesPerYearObject) {
                matchesPerYearObject[match.season] += 1
            }
            else {
                matchesPerYearObject[match.season] = 1
            }
        }

        return matchesPerYearObject
    
}

module.exports = matchesPerYear
