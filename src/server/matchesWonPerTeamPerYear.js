function matchesWonPerTeamPerYear(matches) {
    
    matchesWon = {}

    for (match of matches) {
        if (match.season in matchesWon) {

            if (match.winner in matchesWon[match.season]) {
                matchesWon[match.season][match.winner] += 1
            }
            else {
                matchesWon[match.season][match.winner] = 1
            }
        }

        else {
            matchesWon[match.season] = {}
            matchesWon[match.season][match.winner] = 1
        }
    }

    return matchesWon

}

module.exports = matchesWonPerTeamPerYear